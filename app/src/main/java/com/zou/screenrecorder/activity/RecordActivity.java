package com.zou.screenrecorder.activity;

import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.VideoView;

import com.zou.screenrecorder.R;
import com.zou.screenrecorder.utils.Constant;
import com.zou.screenrecorder.utils.Tools;
import com.zou.screenrecorder.view.VideoControllerView;


import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by zou on 2017/12/15.
 */

public class RecordActivity extends AppCompatActivity {
    private static final String TAG = "RecordActivity";
    private VideoView videoView;
    private String video_path;
    public static final int EVENT_BUS_CONTROLLER_SHOW = 0;
    private VideoControllerView videoControllerView;
    private Chronometer current,total;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();
    private Toolbar toolbar;
    private int currentPosition = 100;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //全屏显示
//        EventBus.getDefault().register(this);

        setContentView(R.layout.activity_record);
        initView();
        setListener();
        init();
    }

    /**
     * 隐藏虚拟键
     */
    private void hideVirtual(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
    /**
     * 显示虚拟键
     */
    private void showVirtual(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN| View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.seekTo(currentPosition);
        hideVirtual();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        videoView.pause();
        currentPosition = videoView.getCurrentPosition();
        pauseVideo();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void initView() {
        //初始化播放器
        video_path = getIntent().getStringExtra(Constant.INTENT_RECORD_URI);
        videoView = findViewById(R.id.videoView);
        videoControllerView = findViewById(R.id.video_controller);
        current = findViewById(R.id.chronometer_current);
        total = findViewById(R.id.chronometer_total);
        //初始化控制界面
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        videoView.setVideoPath(video_path);
    }

    private void init() {
        startVideo();
    }

    private void setListener() {
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(videoView.isPlaying()){
                    pauseVideo();
                }else{
                    startVideo();
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoControllerView.show();
                showVirtual();
                videoControllerView.playOrStop(true);
                toolbar.setVisibility(View.VISIBLE);
                stopTimer();
                mp.seekTo(100);
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                total.setBase(SystemClock.elapsedRealtime() - mp.getDuration());
            }
        });
    }

    private void startVideo(){
        videoView.start();
        videoControllerView.hide();
        hideVirtual();
        videoControllerView.playOrStop(false);
        toolbar.setVisibility(View.GONE);
        startTimer();
    }

    private void pauseVideo(){
        videoView.pause();
        videoControllerView.show();
        showVirtual();
        videoControllerView.playOrStop(true);
        toolbar.setVisibility(View.VISIBLE);
        stopTimer();
    }

    private void startTimer(){
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        videoControllerView.setProgress(videoView.getCurrentPosition()*100/videoView.getDuration());
                        current.setBase(SystemClock.elapsedRealtime()-videoView.getCurrentPosition());
                    }
                });
            }
        };
        timer.schedule(timerTask,0,200);
    }

    private void stopTimer(){
        if(timer!=null){
            timer.cancel();
            timer = null;
        }
        if(timerTask!=null){
            timerTask.cancel();
            timerTask = null;
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                Tools.shareVideo(this,video_path);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}