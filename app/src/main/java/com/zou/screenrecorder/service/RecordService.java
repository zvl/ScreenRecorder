package com.zou.screenrecorder.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.zou.screenrecorder.R;
import com.zou.screenrecorder.utils.Tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;

public class RecordService extends Service {
    private static final String TAG = "RecordService";

    private MediaProjection mediaProjection;
    private MediaRecorder mediaRecorder;
    private VirtualDisplay recordVirtualDisplay, captureVirtualDisplay;

    private boolean running;
    private int realWidth;
    private int realHeight;
    private int dpi;
    private ImageReader mImageReader;
    //这里截屏图片和录像共用一个文件名（不包含后缀），但是在不同的文件夹中
    private String fileName;
    private RecordCallBack recordCallBack;
    private Handler handler;

    private int resolution_rate;//偏好设置中的分辨率
    private float bit_rate;//偏好设置中的码率
    private int video_frame;//偏好设置中的帧率
    public static final String ACTION_CONTROL = "action_control";
    private static final String INTENT_CONTROL = "intent_control";
    private static final int ACTION_CONTROL_SWITCH = 1;
    private ControlReceiver controlReceiver;
    private OnControlCallback onControlCallback;
    private RemoteViews remoteViews;
    private NotificationManager notificationManager;
    private int duration;
    private Timer timer;
    private TimerTask timerTask;
    private boolean audioRecord;

    @Override
    public IBinder onBind(Intent intent) {
        init();
        return new RecordBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void init() {
        remoteViews = new RemoteViews(getPackageName(), R.layout.view_notify);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        startForeground(1, getNotification());
        running = false;
        mediaRecorder = new MediaRecorder();
        handler = new Handler();
        resolution_rate = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("sync_frequency_resolution", "100"));
        bit_rate = Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(this).getString("sync_frequency_bit_rate", "24"));
        video_frame = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("sync_frequency_frame", "30"));
        audioRecord = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("record_MIC",false);
        Log.i(TAG, "分辨率 ： " + resolution_rate + " 码率 ： " + bit_rate + " 帧率 : " + video_frame + "FPS" + " 录制MIC声音："+ audioRecord);
        initReceiver();
    }

    private void initReceiver() {
        controlReceiver = new ControlReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_CONTROL);
        registerReceiver(controlReceiver, intentFilter);
    }



    private Notification getNotification() {
        Intent controlIntent = new Intent(ACTION_CONTROL);
        controlIntent.putExtra(INTENT_CONTROL,ACTION_CONTROL_SWITCH);
        PendingIntent controlPendIntent = PendingIntent.getBroadcast(this, 1, controlIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ib_control,controlPendIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Sharingan_record_channel_id", "name", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null,null);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
            Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_launcher_other_round);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Notification notification = new Notification.Builder(this, "Sharingan_record_channel_id")
                    .setOngoing(true)
                    .setCustomContentView(remoteViews)
                    .setLargeIcon(bitmap)
                    .setSmallIcon(R.drawable.ic_notify_small_icon)
                    .build();
            return notification;
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, DEFAULT_CHANNEL_ID);
            Notification notification = builder.setOngoing(true)
                    .setOngoing(true)
                    .setCustomContentView(remoteViews)
                    .setSmallIcon(R.drawable.ic_notify_small_icon)
                    .build();
            notification.defaults=0;
            return notification;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(handler!=null) {
            handler.removeCallbacksAndMessages(null);
        }
        if(mediaRecorder!=null){
            mediaRecorder.release();
        }
    }

    public void setMediaProject(MediaProjection project) {
        mediaProjection = project;
    }

    public boolean isRunning() {
        return running;
    }

    public void setConfig(int width, int height, int dpi) {
        this.dpi = dpi;
        this.realWidth = width * resolution_rate / 100;
        this.realHeight = height * resolution_rate / 100;
    }

    public void setRecordCallBack(RecordCallBack recordCallBack) {
        this.recordCallBack = recordCallBack;
    }

    /**
     * 开始录制
     *
     * @return
     */
    public boolean startRecord() {
        if (mediaProjection == null || running) {
            return false;
        }
        //开始计时
        startTimer();

        new Thread() {
            @Override
            public void run() {
                initRecorder();
                createVirtualDisplay();
                mediaRecorder.start();
                captureScreen();
                running = true;

                if (recordCallBack != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            //界面开始动画
                            recordCallBack.onStart();
                            //开始通知栏时间的倒计时
                            remoteViews.setChronometer(R.id.ch_notify_time,SystemClock.elapsedRealtime(),null,true);
                            remoteViews.setTextViewText(R.id.tv_notify,getResources().getText(R.string.recording));
                            remoteViews.setImageViewResource(R.id.ib_control,R.drawable.ic_notify_stop);
                            notificationManager.notify(1,getNotification());
                        }
                    });
                }
            }
        }.start();
        return true;
    }

    private void startTimer() {
        timer = new Timer();
        timerTask = new TimerTask() {
                @Override
                public void run() {
                    duration++;
                }
            };
        timer.schedule(timerTask,0,1000);
    }

    private void stopTimer(){
        if(timer!=null){
            timer.cancel();
            timer = null;
        }
        duration = 0;
    }


    /**
     * 停止录制
     *
     * @return
     */
    public boolean stopRecord() {
        if (!running) {
            return false;
        }
        new Thread() {
            @Override
            public void run() {
                running = false;
                recordVirtualDisplay.release();
                mediaProjection.stop();
                mediaRecorder.setOnErrorListener(null);
                mediaRecorder.setOnInfoListener(null);
                mediaRecorder.setPreviewDisplay(null);
                mediaRecorder.stop();
                mediaRecorder.reset();
                if (recordCallBack != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            remoteViews.setChronometer(R.id.ch_notify_time,SystemClock.elapsedRealtime(),"00:00",false);
                            remoteViews.setTextViewText(R.id.tv_notify,getResources().getText(R.string.record_stopped));
                            remoteViews.setImageViewResource(R.id.ib_control,R.drawable.ic_notify_play);
                            notificationManager.notify(1,getNotification());
                            renameFile();
                            stopTimer();
                            recordCallBack.onStop();
                        }
                    });
                }
            }
        }.start();
        return true;
    }

    private void renameFile() {
        File fileVideo = new File(Tools.getSaveRecordDirectory()+fileName+".mp4");
        File fileImage = new File(Tools.getSaveImageDirectory(getApplicationContext())+fileName+".mp4");
        fileVideo.renameTo(new File(Tools.getSaveRecordDirectory()+fileName+"_"+duration+".mp4"));
        fileImage.renameTo(new File(Tools.getSaveImageDirectory(getApplicationContext())+fileName+"_"+duration+".mp4"));
    }

    /**
     * 创建虚拟屏幕
     */
    private void createVirtualDisplay() {
        mImageReader = ImageReader.newInstance(realWidth, realHeight, PixelFormat.RGBA_8888, 1);
        if (mImageReader != null) {
            Log.i(TAG, "imageReader Successful");
        }
        recordVirtualDisplay = mediaProjection.createVirtualDisplay("MainScreen", realWidth, realHeight, dpi,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mediaRecorder.getSurface(), null, null);
        captureVirtualDisplay = mediaProjection.createVirtualDisplay("MainScreen", realWidth, realHeight, dpi,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mImageReader.getSurface(), null, null);
    }

    /**
     * 初始化录制
     */
    private void initRecorder() {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
            fileName = dateFormat.format(new java.util.Date());
            if(audioRecord) {
                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            }
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setOutputFile(Tools.getSaveRecordDirectory() + fileName + ".mp4");
            mediaRecorder.setVideoSize(realWidth , realHeight);
            mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            if(audioRecord) {
                mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            }
            mediaRecorder.setVideoEncodingBitRate((int) (bit_rate * video_frame * 1024 * 1024));
            mediaRecorder.setVideoFrameRate(video_frame);
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 截屏
     */
    private void captureScreen() {
        String pathImage = Tools.getSaveImageDirectory(getApplicationContext());

        String nameImage = pathImage + fileName + ".png";
        SystemClock.sleep(1000);
        Image image = mImageReader.acquireLatestImage();
        int width = image.getWidth();
        int height = image.getHeight();
        final Image.Plane[] planes = image.getPlanes();
        final ByteBuffer buffer = planes[0].getBuffer();
        int pixelStride = planes[0].getPixelStride();
        int rowStride = planes[0].getRowStride();
        int rowPadding = rowStride - pixelStride * width;
        Bitmap bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height);
        image.close();
        if (captureVirtualDisplay == null) {
            return;
        }
        captureVirtualDisplay.release();
        captureVirtualDisplay = null;
        storeBitmap(nameImage, bitmap);
    }

    /**
     * 储存bitmap到CacheDir中
     */
    private void storeBitmap(String path, Bitmap bitmap) {
        try {
            File file = new File(path);
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class RecordBinder extends Binder {
        public RecordService getRecordService() {
            return RecordService.this;
        }
    }

    public interface RecordCallBack {
        void onStart();

        void onStop();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mediaRecorder.release();
        unregisterReceiver(controlReceiver);
        return super.onUnbind(intent);
    }

    public class ControlReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(ACTION_CONTROL)){
                int actionId = intent.getIntExtra(INTENT_CONTROL, -1);
                switch (actionId){
                    case ACTION_CONTROL_SWITCH:
                        //播放控制
                        if(onControlCallback!=null){
                            onControlCallback.controlCallback();
                        }
                        break;
                }
            }
        }
    }

    public interface OnControlCallback{
        void controlCallback();
    }

    public void setOnControlCallback(OnControlCallback onControlCallback){
        this.onControlCallback = onControlCallback;
    }


}
