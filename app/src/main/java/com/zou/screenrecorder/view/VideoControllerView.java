package com.zou.screenrecorder.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.zou.screenrecorder.R;

public class VideoControllerView extends ConstraintLayout {
    private ImageView iv_play;
    private ProgressBar progress;
    private View root;
    public VideoControllerView(Context context) {
        this(context,null);
    }

    public VideoControllerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public VideoControllerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        root = LayoutInflater.from(context).inflate(R.layout.view_video_controller,this,true);
        iv_play = root.findViewById(R.id.iv_play);
        progress = root.findViewById(R.id.progress);
//        iv_play.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(playing){
//                    videoEvent.onPause();
//                }else{
//                    videoEvent.onPlay();
//                }
//            }
//        });
    }

    public void show(){
        iv_play.setVisibility(VISIBLE);
        setBackgroundColor(Color.parseColor("#cc000000"));
//        root.setBackgroundColor(Color.parseColor("#cc000000"));
    }

    public void hide(){
        iv_play.setVisibility(INVISIBLE);
        setBackgroundColor(Color.parseColor("#00000000"));
//        root.setBackgroundColor(Color.parseColor("#ff000000"));
    }

    public void playOrStop(boolean play){
        if(play){
            iv_play.setImageResource(R.mipmap.ic_play);
        }else{
            iv_play.setImageResource(R.mipmap.ic_pause);
        }
    }

    public void setProgress(int progress){
        this.progress.setProgress(progress);
    }

//    public void setVideoEvent(VideoEvent videoEvent){
//        this.videoEvent = videoEvent;
//    }
//
//    public interface VideoEvent{
//        void onPlay();
//        void onPause();
//    }
}
